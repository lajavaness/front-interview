import { STARTUP, STARTUP_FAILURE, STARTUP_SUCCESS } from '../actions'

import initialState from '../store/initialState'

export default (state = initialState, action) => {
	switch (action.type) {
		case STARTUP:
			return { ...state, isLoading: true }
		case STARTUP_SUCCESS:
			return { ...state, isLoading: false }
		case STARTUP_FAILURE:
			return { ...state, error: action.payload.error }
		default:
			return state
	}
}
