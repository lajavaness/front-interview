import React, { Component, Fragment } from 'react'
import { Router } from 'react-router-dom'
import createBrowserHistory from 'history/createBrowserHistory'

import './__styles__/App.css'

const history = createBrowserHistory()

class App extends Component {
	render() {
		return (
			<Router history={history}>
				<Fragment>
					<header className="App-header">
						<img src={process.env.PUBLIC_URL + '/logo.png'} className="App-logo" alt="logo" />
					</header>
				</Fragment>
			</Router>
		)
	}
}

export default App
