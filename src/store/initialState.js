import faker from 'faker'

export default {
	isLoading: false,
	products: Array(10).fill().map(e => ({
		id: faker.random.uuid(),
		name: faker.commerce.productName(),
		price: faker.commerce.price(),
		image: faker.image.imageUrl()
	})),
	error: null,
}
