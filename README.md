La Javaness : Front Trial

### Test technique ReactJS

À partir de cette base de développement et des librairies qui la composent, le but du test est de réaliser une interface e-commerce simple.

Les écrans attendus sont les suivants :

**Catalogue** :

   * Une liste de produits. Les produits de la liste contiennent une vignette, un nom, un prix (HT) et un bouton d'ajout/suppression.
   * Un encart indique le nombre d'articles sélectionnés et le prix total mis à jour à chaque ajout/suppression d'un produit.
   * Un bouton permet de valider la sélection et de passer au panier.

**Panier** :

   * Une liste de produits. Les produits de la liste contiennent un nom, un prix (HT) et un bouton d'ajout/suppression.
   * Le prix total fait apparaître le prix additionné HT de tous les articles et la TVA (20%).
   * Un champ permet de saisir un code de réduction. À la validation de ce champ, une réduction de 5% sur le prix HT est appliquée tous les 3 articles.
   * Un bouton permet de revenir au catalogue.
   * Un bouton permet de valider le panier et de passer à la conclusion.

**Conclusion** :

   * Un message de remerciement.
   * Un bouton permet de revenir au catalogue, le panier vide.

Une liste de produits est générée aléatoirement dans `src/store/initialState.js`.
Elle est fournie à titre d'exemple et peut parfaitement être remplacée par une autre, provenant d'une API par exemple.

Il est possible de ne pas utiliser toutes les librairies proposées et n’importe quelle autre librairie tierce peut être ajoutée.

Aucune contrainte de temps n'est fixée.

